package com.ajh.springdemo;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/registration")
public class RegistrationController {
	private static final org.slf4j.Logger logger = 
			org.slf4j.LoggerFactory.getLogger(RegistrationController.class);

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private UserValidator userValidator;

	@GetMapping
	public String registration(Model model) {
		model.addAttribute("user", new User() {{
			setName("andy");
			setEmail("andy.h.jiang@gmail.com");
			setPassword("1234");
		}});
		return "registration";
	}

	@PostMapping
	public String doRegistration(@Valid User user, BindingResult result) {
		logger.debug("Registering user: " + user);

		userValidator.validate(user, result);
		if (result.hasErrors()) {
			return "registration";
		}

		userRepo.save(user);

		return "redirect:/";
	}
}
